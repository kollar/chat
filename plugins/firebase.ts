import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

export default defineNuxtPlugin((nuxtApp) => {
    const firebaseConfig = {
        apiKey: "AIzaSyCNfmTdOARSPzNpbOe-Ifne7bMrLlVOwTM",
        authDomain: "chat-kollar.firebaseapp.com",
        projectId: "chat-kollar",
        storageBucket: "chat-kollar.appspot.com",
        messagingSenderId: "279490069316",
        appId: "1:279490069316:web:cccc3ceff7efc5032d0d11"
    };

    const app = initializeApp(firebaseConfig);
    const firestore = getFirestore(app);

    return {
        provide: {
            firestore
        }
    }
})
